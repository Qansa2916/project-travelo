import Link from "next/link";
type Props = {
  lable: string;
  href: string;
  className?: string;
};
const NavItem = ({ lable, href = "/", className="" }: Props) => {
  return (
    <Link href={href}>
      <a className="tect-heading-4 text-gray-70 font-label font-bold">
        {lable}
      </a>
    </Link>
  );
};

export default NavItem;
