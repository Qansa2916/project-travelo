import React from 'react'
import NavItem from '../NavItem'

const Navbar = () => {
  return (
    <div className='flex items-center gap-5'>
      <NavItem href='/' lable='Beranda' />
      <NavItem href='/' lable='Kontak' />
      <NavItem href='/' lable='Paket' />
      <NavItem href='/' lable='Testimonial' />

    </div>
  )
}

export default Navbar